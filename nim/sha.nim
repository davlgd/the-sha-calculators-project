import std/md5, std/sha1, nimSHA2, json, std/strutils

let toHash = stdin.readAll

proc toHex(hash: SecureHash): string =
  result = ""
  let hashArray = cast[array[20, byte]](hash)
  for i in 0..<20:
    result.add(hashArray[i].toHex(2))

let jsonObj = %*{
  "MD5": getMD5(toHash),
  "SHA-1": secureHash(toHash).toHex,
  "SHA-256": computeSHA256(toHash).toHex,
  "SHA-512":  computeSHA512(toHash).toHex
}

echo jsonObj