import os
import json
import crypto.md5
import crypto.sha1
import crypto.sha256
import crypto.sha512

// Define a struct to hold hash results
struct Hashes {
    md5 string [json:'MD5']
    sha1 string [json:'SHA-1']
    sha256 string [json:'SHA-256']
    sha512 string [json:'SHA-512']
}

fn main() {
    stdin := read_stdin().bytestr()
    
    // Calculate hashes and passing them to a struct
    hashes := Hashes{
        md5: md5.hexhash(stdin)
        sha1: sha1.hexhash(stdin)
        sha256: sha256.hexhash(stdin)
        sha512: sha512.hexhash(stdin)
    }
    
    // Show the result JSON formated
    println(json.encode(hashes))
}

// A function reading the full stdin
fn read_stdin() []u8 {
    mut content := []u8{}

    for {
        buffer := os.get_raw_stdin()
        if buffer.len == 0 { break }
        content << buffer
    }

    return content
}
