#include <iostream>
#include <iomanip>
#include <sstream>
#include <openssl/evp.h>
#include <nlohmann/json.hpp>

std::string bytesToHex(const unsigned char* bytes, unsigned int len) {
    std::stringstream ss;
    for(unsigned int i = 0; i < len; i++)
    {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)bytes[i];
    }
    return ss.str();
}

std::string calculateHash(const std::string& str, const EVP_MD* hashType) {
    EVP_MD_CTX *ctx;
    if((ctx = EVP_MD_CTX_new()) == nullptr) {
        std::cerr << "Unable to create new EVP_MD_CTX" << std::endl;
        return "";
    }

    if(EVP_DigestInit_ex(ctx, hashType, nullptr) != 1) {
        std::cerr << "Unable to initialise EVP Digest" << std::endl;
        EVP_MD_CTX_free(ctx);
        return "";
    }

    if(EVP_DigestUpdate(ctx, str.c_str(), str.size()) != 1) {
        std::cerr << "Unable to continue EVP Digest" << std::endl;
        EVP_MD_CTX_free(ctx);
        return "";
    }

    unsigned char hash[EVP_MAX_MD_SIZE];
    unsigned int lengthOfHash = 0;
    if(EVP_DigestFinal_ex(ctx, hash, &lengthOfHash) != 1) {
        std::cerr << "Unable to finalise EVP Digest" << std::endl;
        EVP_MD_CTX_free(ctx);
        return "";
    }

    EVP_MD_CTX_free(ctx);

    return bytesToHex(hash, lengthOfHash);
}

int main() {
    std::string input((std::istreambuf_iterator<char>(std::cin)), std::istreambuf_iterator<char>());

    nlohmann::json j;
    j["MD5"] = calculateHash(input, EVP_md5());
    j["SHA1"] = calculateHash(input, EVP_sha1());
    j["SHA256"] = calculateHash(input, EVP_sha256());
    j["SHA512"] = calculateHash(input, EVP_sha512());

    std::cout << j.dump(4) << std::endl;
    return 0;
}