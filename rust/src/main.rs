use ring::digest::{SHA512, SHA256, digest};
use md5::compute as md5_compute;
use data_encoding::HEXUPPER;
use serde_derive::Serialize;
use std::io::{self, Read};
use sha1::{Sha1, Digest};

#[derive(Serialize)]
struct Hashes<'a> {
    md5: &'a str,
    sha1: &'a str,
    sha256: &'a str,
    sha512: &'a str,
}

fn main() {
    let mut data = Vec::new();
    io::stdin().read_to_end(&mut data).expect("Failed to read data from stdin");

    let hashes = Hashes {
        md5: &hash_md5(&data),
        sha1: &hash_sha1(&data),
        sha256: &hash_sha256(&data),
        sha512: &hash_sha512(&data),
    };

    serde_json::to_writer(io::stdout(), &hashes).expect("Failed to write hashes to stdout");
}

fn hash_md5(data: &[u8]) -> String {
    HEXUPPER.encode(md5_compute(data).as_ref())
}

fn hash_sha1(data: &[u8]) -> String {
    let mut sha1 = Sha1::new();
    sha1.update(data);
    HEXUPPER.encode(sha1.finalize().as_slice())
}

fn hash_sha256(data: &[u8]) -> String {
    HEXUPPER.encode(digest(&SHA256, data).as_ref())
}

fn hash_sha512(data: &[u8]) -> String {
    HEXUPPER.encode(digest(&SHA512, data).as_ref())
}