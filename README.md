# The Sha calculators project

A personal project to compare source code length and binary size built from different languages. The resulting tool reads raw data from stdin, calculate its md5, sha1, sha256 and sha512 sums, and send results in a JSON object through stdout. 

Here are the binary size without optimization (debug style), with optimizations from the compiler (release style) and compressed with external tools. Test system architecture is x86_64, OS is Ubuntu 23.04 : 

| Language | Raw  | Optim | Strip | UPX  |
| -------- |:----:|:-----:|:-----:|:----:|
| C++      | 316K | 51K   | 51K   | 21K  |
| Go       | 2,2M | 1,5M  | 1,5M  | 632K |
| Nim      | 289K | 97K   | 71K   | 44K  |
| Rust     | 9,4M | 355K  | 355K  | 157K |
| V        | 1,2M | 85K   | 101K  | 177K |

* **C++ :**

```
g++ sha.cpp -lcrypto
g++ -Os -s sha.cpp -lcrypto
```

* **Go :**

```
go build sha.go
go build -ldflags "-s -w" sha.go
```

* **Nim :**

```
nim c sha.nim
nim c -d:release --opt:size sha.nim
```

* **Rust :**

```
cargo build
cargo build --release
```
In Cargo.toml add :
```
[profile.release]
lto=true
strip=true
opt-level='z'
```

* **V :**

```
v sha.v
v -prod -compress sha.v
```
In V I'm not able to read the full `stdin` (only until the first empry line), so the filename is passed as an argument.

* **Strip :**

```
strip hash 
```

* **UPX :**

```
upx --best hash 
```

UPX and Strip are applied on the optimized build, except for V (it led to an error as `-compress` perform the same action, I use a `-prod` build))