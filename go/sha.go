package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"hash"
)

type Hashes struct {
	MD5    string `json:"MD5"`
	SHA1   string `json:"SHA1"`
	SHA256 string `json:"SHA256"`
	SHA512 string `json:"SHA512"`
}

func main() {
	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read from stdin: %v\n", err)
		os.Exit(1)
	}

	hashes := Hashes{
		MD5:    hashData(md5.New(), data),
		SHA1:   hashData(sha1.New(), data),
		SHA256: hashData(sha256.New(), data),
		SHA512: hashData(sha512.New(), data),
	}

	err = json.NewEncoder(os.Stdout).Encode(hashes)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to encode to JSON: %v\n", err)
		os.Exit(1)
	}
}

func hashData(hasher hash.Hash, data []byte) string {
	hasher.Write(data)
	return hex.EncodeToString(hasher.Sum(nil))
}